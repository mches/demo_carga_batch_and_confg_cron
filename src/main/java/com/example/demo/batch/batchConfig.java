package com.example.demo.batch;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import com.example.demo.batch.listener.JobCompletionNotificationListener;
import com.example.demo.beans.StateEmergency;
import com.example.demo.beans.StateEmergencyItemProcessor;

@Configuration
@EnableBatchProcessing
public class batchConfig {
	
	 @Autowired
	  private JobBuilderFactory jobBuilderFactory;
	  @Autowired
	  private StepBuilderFactory stepBuilderFactory;
	  @Autowired
	  private DataSource dataSource;
//	 
//
//	 	
	 @Bean
	 public FlatFileItemReader<StateEmergency> reader() {
		 
		 FlatFileItemReader<StateEmergency> reader = new FlatFileItemReader<>();
		 	reader.setResource(new FileSystemResource("C:/usrappcc//local/data.csv"));
		    //reader.setResource(new ClassPathResource("data.csv"));
		    reader.setLineMapper(new DefaultLineMapper<StateEmergency>() {{
		      setLineTokenizer(new DelimitedLineTokenizer() {{
		        setNames(new String[]{"tipoDocumento", "numeroDocumento","servicio","estado"});
		      }});
		      setFieldSetMapper(new BeanWrapperFieldSetMapper<StateEmergency>() {{
		        setTargetType(StateEmergency.class);
		      }});
		    }});
		    return reader;
	 }
 
	 @Bean
	 public StateEmergencyItemProcessor processor() {
	   return new StateEmergencyItemProcessor();
	 }
	 
	 
	 @Bean
	 public JdbcBatchItemWriter<StateEmergency> writer() {
		 
		 JdbcBatchItemWriter<StateEmergency> writer = new JdbcBatchItemWriter<>();
		    writer.setItemSqlParameterSourceProvider(
		        new BeanPropertyItemSqlParameterSourceProvider<StateEmergency>());
		    writer.setSql("INSERT INTO emergencia_tmp (tipo_documento, numero_documento,servicio,estado) VALUES (:tipoDocumento, :numeroDocumento,:servicio,:estado)");
		    writer.setDataSource(dataSource);
		    return writer;
		 
	 }
	 
	 @Bean
	  public Job importStateEmergencyJob(JobCompletionNotificationListener listener) {
	    return jobBuilderFactory.get("importStateEmergencyJob").incrementer(new RunIdIncrementer())
	        .listener(listener).flow(stepStateEmergency()).end().build();
	  }
	 
	  @Bean
	  public Step stepStateEmergency() {
	    return stepBuilderFactory.get("stepStateEmergency").<StateEmergency, StateEmergency>chunk(10).reader(reader())
	        .processor(processor()).writer(writer()).build();
	  }

	 

}
