package com.example.demo.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class StringUtil {
	
	public static String getFechaHoraActual() {
		final String date = new SimpleDateFormat("yyyyMMdd",
				Locale.getDefault()).format(Calendar.getInstance().getTime());
		
		return date;
	}

}
