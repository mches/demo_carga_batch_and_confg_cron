package com.example.demo.beans;

public class StateEmergency {
	
	public StateEmergency(String tipoDocumento, String numeroDocumento, String servicio, String estado) {
		super();
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
		this.servicio = servicio;
		this.estado = estado;
	}
	private String tipoDocumento;
	private String numeroDocumento;
	private String servicio;
	private String estado;
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public StateEmergency() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
