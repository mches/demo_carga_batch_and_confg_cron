package com.example.demo.task;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.demo.service.SftpService;
import com.example.demo.util.StringUtil;

@Component
public class loadEEFile {
	
	@Autowired
	private JobLauncher jobLauncher;
     
    @Autowired
    private Job job;
	
    @Autowired
	private SftpService sftpService;
	
	//@Scheduled (cron = "${cron.expression}")
	@Scheduled(fixedRate = 30000, initialDelay = 10000)
	public void cargarArchivoEE() {
		
		
		
		
		String fileNameFind="bloqueos_covid_"+ StringUtil.getFechaHoraActual();
		String urlLocal="C://usrappcc//local//data.csv";
		String urlRemote="/test/";
		sftpService.downloadFileEE(fileNameFind, urlLocal, urlRemote);
		
		JobParameters params = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();
	    try {
			jobLauncher.run(job, params);
		} catch (JobExecutionAlreadyRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobRestartException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobInstanceAlreadyCompleteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobParametersInvalidException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
