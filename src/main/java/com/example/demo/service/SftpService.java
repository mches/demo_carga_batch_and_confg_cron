package com.example.demo.service;

import java.util.Vector;

import org.springframework.stereotype.Component;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

@Component
public class SftpService {
	
	
	public Boolean downloadFileEE(String fileNameFind,String urlLocal,String urlRemote) {
		
		Boolean succes= false;
		Session remoteSession=getSession();
		ChannelSftp remoteChannel=getChannel(remoteSession);
		
		String fileNameToDownload= null;
		
		try {
			Vector v= remoteChannel.ls(urlRemote);
			fileNameToDownload=fileRemoteFind(v,fileNameFind);
			remoteChannel.get(urlRemote+fileNameToDownload,urlLocal);
			succes=true;
		} catch (SftpException e) {
			e.printStackTrace();
		}
		finally {
			remoteChannel.disconnect();
			remoteSession.disconnect();
		}
		return succes;
	}

	private Session getSession()  {
		JSch jsch = new JSch();
		Session session = null;
		try {
			session = jsch.getSession("mestrada", "127.0.0.1", 22);
			session.setConfig("StrictHostKeyChecking", "si");
	         session.setPassword("mestrada");
	         session.connect();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return session;
	}
	
	
	private ChannelSftp getChannel(Session session) {
		
		Channel channel;
		ChannelSftp sftpChannel = null;
		try {
			channel = session.openChannel("sftp");
			channel.connect();
	        sftpChannel = (ChannelSftp) channel;
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sftpChannel;
        
	}
	
	private String fileRemoteFind(Vector FileRemoteList,String fileNameFind) {
		
		String fileNameFound=null;
		
		for(int i=0; i<FileRemoteList.size();i++){
     	   LsEntry entry = (LsEntry) FileRemoteList.get(i);
     	   
     	  String fileNameRemote= entry.getFilename();
     	  if(fileNameRemote.contains(fileNameFind)) {
     		 fileNameFound=fileNameRemote;
     		 continue;
     	  }
        }
		return fileNameFound;
	}
	
}
