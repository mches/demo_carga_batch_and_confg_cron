CREATE TABLE `claro_bd`.`emergencia_tmp` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tipo_documento` VARCHAR(45) NOT NULL,
  `numero_documento` VARCHAR(45) NOT NULL,
  `servicio` VARCHAR(45) NOT NULL,
  `estado` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`));
  
  
CREATE TABLE `claro_bd`.`emergencia` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tipo_documento` VARCHAR(45) NOT NULL,
  `numero_documento` VARCHAR(45) NOT NULL,
  `servicio` VARCHAR(45) NOT NULL,
  `estado` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`));
  
  
  
  
USE `claro_bd`;
DROP procedure IF EXISTS `carga_data_procedure`;

DELIMITER $$
USE `claro_bd`$$
CREATE PROCEDURE `carga_data_procedure` ()
BEGIN

Insert into emergencia (tipo_documento,numero_documento,servicio,estado)
select tmp.tipo_documento, tmp.numero_documento, tmp.servicio, tmp.estado
from emergencia_tmp tmp where tmp.estado=1;

update emergencia
set estado=0
where numero_documento in (select numero_documento from emergencia_tmp where estado=0 ) and 
      servicio in (select servicio from emergencia_tmp where estado=0 );
      
      
END$$

DELIMITER ;